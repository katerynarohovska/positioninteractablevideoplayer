//
//  ViewController.swift
//  EventInteractablePlayer
//
//  Created by Catherine Rohovska on 12.01.2022.
//

import UIKit

class ViewController: UIViewController {
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let view = EventInteractablePlayer(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height))
        view.backgroundColor = .lightGray
        self.view.addSubview(view)
        //to track events, view needs to become first responder
        view.becomeFirstResponder()
        if let videoURL = URL(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WeAreGoingOnBullrun.mp4") {
            view.loadVideo(url: videoURL)
        }
        
    }


}

