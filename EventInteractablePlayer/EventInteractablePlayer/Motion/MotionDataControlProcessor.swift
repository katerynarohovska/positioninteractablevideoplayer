//
//  MotionDataControlProcessor.swift
//  EventInteractablePlayer
//
//  Created by Catherine Rohovska on 13.01.2022.
//

import Foundation
import CoreLocation

enum ControlAction {
    case replay
    case encreaseVolume
    case decreaseVolume
    case backward
    case forward
}

protocol MotionDataControlProcessorDelegate: AnyObject {
    func call(action: ControlAction)
}

final class MotionDataControlProcessor: NSObject {
    static let distanceFilter = 10.0 // 10 meters distance, maybe lack of precision in location manager itself
    
    lazy var locationManager: CLLocationManager = {
        [unowned self] in
        let _locationManager = CLLocationManager()
        _locationManager.delegate = self
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest
        _locationManager.distanceFilter = MotionDataControlProcessor.distanceFilter
        _locationManager.pausesLocationUpdatesAutomatically = false
        _locationManager.activityType = .other
        return _locationManager
    }()
    
    private var initialPosition: CLLocation?
    private var prevPosition: Motion2DPosition?
    weak var delegate: MotionDataControlProcessorDelegate?
    
    override init() {
        super.init()
        MotionManager.shared.addReceiver(newReceiver: self)
        
        switch self.locationManager.authorizationStatus {
            
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
        case .authorizedAlways:
            locationManager.startUpdatingLocation()
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
        default:
            break
        }
    }
    
    deinit {
        MotionManager.shared.removeReceiver(receiver: self)
    }
}


// MARK: -- MotionManagerDelegate

extension MotionDataControlProcessor: MotionManagerDelegate {
    func didUpdate(position: Motion2DPosition) {
        // if we have no previous position, set it and look for update
        guard let lastPosition = prevPosition else {
            prevPosition = position
            return
        }
        
        // set "blind area" from -20 to 20 degree each direction
        // returning to it means stop of changes
        let blindRange = -20.0...20
        
        if (blindRange.contains(position.pitch) && (blindRange.contains(position.yaw))) {
            // no action required
            return
        }
        
        // detect direction
        let filterTrashold = 90.0 // avoid suspisious values and lock position to 90 degree both sides
        let deltaPitch = abs(position.pitch - lastPosition.pitch)
        let deltaYaw = abs(position.yaw - lastPosition.yaw)
        let isPitch = !blindRange.contains(position.pitch) && deltaPitch < filterTrashold
        let isYaw = !blindRange.contains(position.yaw) && deltaYaw < filterTrashold
        
        //select action
        if isPitch, deltaPitch > deltaYaw {
            
            let isDirectionPositive = position.pitch > 0
            
            delegate?.call(action: isDirectionPositive ? .encreaseVolume : .decreaseVolume)
            
        }
        
        if isYaw, deltaPitch < deltaYaw {
            
            let isDirectionPositive = position.yaw > 0
            
            delegate?.call(action: isDirectionPositive ? .backward : .forward)
            
        }
        
    }
}


// MARK: -- CLLocationManagerDelegate

extension MotionDataControlProcessor: CLLocationManagerDelegate {
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        
        switch manager.authorizationStatus
        {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
        default:
            break
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if (initialPosition == nil ) {
            
            initialPosition = locations.first
            return
            
        }
        
        //next event will be generated after moving approximately filterDistance, so
        //may generate replay action and update position
        if let initialLocation = self.initialPosition, let currentLocation = locations.first {
            
            let distance = currentLocation.distance(from: initialLocation)
            
            if (distance >= MotionDataControlProcessor.distanceFilter) {
                
                delegate?.call(action: .replay)
                initialPosition = locations.first
                
            }
        }
        
    }
}
