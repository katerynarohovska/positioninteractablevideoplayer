//
//  MotionManager.swift
//  EventInteractablePlayer
//
//  Created by Catherine Rohovska on 12.01.2022.
//

import Foundation
import CoreMotion

struct Motion2DPosition {
    var pitch: Double
    var yaw: Double
}

protocol MotionManagerDelegate: AnyObject {
    func didUpdate(position: Motion2DPosition)
}

final class MotionManager: NSObject {
    
    static let shared = MotionManager()
    
    private let operationQueue = OperationQueue()
    private var initialState = true
    private let manager = CMMotionManager()
    private var referenceAttitude: CMAttitude?
    private var receivers = [MotionManagerDelegate]()
    
    private override init() {
        
        super.init()
    }
    
    func addReceiver(newReceiver: MotionManagerDelegate) {
        
        receivers.append(newReceiver)
        
    }
    
    func removeReceiver(receiver: MotionManagerDelegate) {
        
        receivers.removeAll {
            $0 === receiver
        }
        
    }
    
    func startTracking() {
        
        manager.deviceMotionUpdateInterval = 0.1
        guard manager.isDeviceMotionAvailable else { return }
        guard !manager.isDeviceMotionActive else { return }
        
        manager.startDeviceMotionUpdates(to: operationQueue) { [weak self] deviceMotion, error in
            
            guard let currentMotion = deviceMotion else { return }
            self?.update(currentAttitude: currentMotion.attitude)
            
        }

    }
    
    func stopTracking() {
        
        manager.stopDeviceMotionUpdates()
        
    }
    
    private func update(currentAttitude: CMAttitude) {
        
        let currentAttitude = currentAttitude
        
        // Z axis -- Yaw, X axis -- pitch
        
        let pitch = MathHelper.radiansToDegrees(currentAttitude.pitch)
        let yaw = MathHelper.radiansToDegrees(currentAttitude.yaw)
        
        for receiver in receivers {
            let position = Motion2DPosition(pitch: pitch, yaw: yaw)
            receiver.didUpdate(position: position)
        }
        
    }
    
}
