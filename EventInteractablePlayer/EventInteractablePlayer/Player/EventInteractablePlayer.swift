//
//  EventInteractablePlayer.swift
//  EventInteractablePlayer
//
//  Created by Catherine Rohovska on 12.01.2022.
//

import UIKit

class EventInteractablePlayer: UIView {
    
    let controlPanel = EventControlPanel()
    private var volumeLabel = UILabel()
    private var durationLabel = UILabel()
    
    private var volumeObserver: Any?
    private var progressObserver: Any?
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.customize()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func willMove(toSuperview: UIView?) {
            super.willMove(toSuperview: toSuperview)

            if toSuperview != nil {
                trackControlParams()
            } else {
                untrackControlParams()
            }
        }
    
}


// MARK: -- Motion events

extension EventInteractablePlayer {
    
    override func motionBegan(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if (motion == .motionShake) {
            // don't track motion between gestures
            MotionManager.shared.stopTracking()
        }
    }
    
    override func motionCancelled(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if (motion == .motionShake) {
            MotionManager.shared.startTracking()
        }
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if (motion == .motionShake) {
            //pause only on successfull gesture
            controlPanel.togglePlaying()
            MotionManager.shared.startTracking()
        }
    }
    
}

// MARK: -- UI + observers

extension EventInteractablePlayer {
    
    func customize() {
        volumeLabel.frame = CGRect(x: 20, y: 60, width: self.bounds.width - 40, height: 20)
        volumeLabel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        durationLabel.frame = CGRect(x: 20, y: 80, width: self.bounds.width - 40, height: 20)
        durationLabel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.addSubview(volumeLabel)
        self.addSubview(durationLabel)
    }
    
    func trackControlParams() {
        volumeObserver = controlPanel.observe(\.volumeLevel, options: [.new]) {[weak self] panel, intValue in
            
            DispatchQueue.main.async {
                self?.volumeLabel.text = "Volume: \(intValue.newValue ?? 0) %"
            }
            
        }
        
        progressObserver = controlPanel.observe(\.durationString,  options: [.new]) { [weak self] panel, value in
            
            DispatchQueue.main.async {
                self?.durationLabel.text = "Progress: \(value.newValue ?? "") sec"
            }
            
        }
    }
    
    func untrackControlParams() {
        volumeObserver = nil
        progressObserver = nil
    }
}


// MARK: -- Video

extension EventInteractablePlayer {
    
    func loadVideo(url: URL) {
        controlPanel.setupPanel(view: self)
        controlPanel.loadVideo(url: url)
    }
    
}
