//
//  EventControlPanel.swift
//  EventInteractablePlayer
//
//  Created by Catherine Rohovska on 12.01.2022.
//

import Foundation
import UIKit
import AVFoundation

final class EventControlPanel: NSObject {
    
    private var player: AVPlayer!
    private var isPlaying = false
    private let dataProcessor = MotionDataControlProcessor()
    // properties to be observed in UI
    @objc dynamic private(set) var durationString: String = ""
    @objc dynamic private(set) var volumeLevel: Int = 0
    
    private var timeObserverToken: Any?
    private var volumeObserver: Any?
    
    weak var view: UIView?
    
    func setupPanel(view: UIView) {
        
        self.view = view
        self.dataProcessor.delegate = self
        
    }
    
    
    func loadVideo(url: URL) {
        
        self.player = AVPlayer(url: url)
        
        let playerLayer = AVPlayerLayer(player: player)
        if let view = self.view {
            playerLayer.frame = view.bounds
            self.view?.layer.addSublayer(playerLayer)
            
            volumeObserver = player.observe(\.volume, options: [.new]) { [weak self] playerItem, volumeValue in
                let percentageVolume = Int((volumeValue.newValue ?? 0) * 100.0)
                self?.volumeLevel = percentageVolume
            }
            
            addPeriodicTimeObserver()
            playVideo()
            
        }
        
    }
    
    
    func addPeriodicTimeObserver() {
        // Notify every half second
        
        let timeScale = CMTimeScale(NSEC_PER_SEC)
        let time = CMTime(seconds: 0.5, preferredTimescale: timeScale)
        
        timeObserverToken = player.addPeriodicTimeObserver(forInterval: time,
                                                           queue: .main) {
            [weak self] time in
            let duration = self?.player.currentItem?.duration ?? .zero
            let timeString = NSString(format: "%.0f/%.0f", round(time.seconds), round(duration.seconds))
            self?.durationString = timeString as String
        }
    }
    
    func removePeriodicTimeObserver() {
        if let timeObserverToken = timeObserverToken {
            player.removeTimeObserver(timeObserverToken)
            self.timeObserverToken = nil
        }
    }
    
    
    deinit {
        removePeriodicTimeObserver()
        volumeObserver = nil
    }
    
}

// MARK: -- Controls

extension EventControlPanel {
    func togglePlaying(){
        if (isPlaying) {
            pauseVideo()
        } else {
            playVideo()
        }
    }
    
    private func playVideo() {
        MotionManager.shared.startTracking()
        player.play()
        isPlaying = true
    }
    
    private func pauseVideo() {
        player.pause()
        isPlaying = false
    }
    
    func increaseVolume() {
        let maximumVolume = Float(1.0)
        let step = Float(0.1)
        var newVolume = player.volume + step
        if (newVolume > maximumVolume) {
            newVolume = maximumVolume
        }
        player.volume = newVolume
    }
    
    func decreaseVolume() {
        let minimumVolume = Float(0.0)
        let step = Float(0.1)
        var newVolume = player.volume - step
        if (newVolume < minimumVolume) {
            newVolume = minimumVolume
        }
        player.volume = newVolume
    }
    
    func seekPlayback(isForward: Bool) {
        let seekDuration: Float64 = 1.0
        guard
            let player = player,
            let duration = player.currentItem?.duration
        else {
            return
        }
        
        let currentTime = player.currentTime().seconds
        
        var destinationTime = isForward ? (currentTime + seekDuration) : (currentTime - seekDuration)
        
        if destinationTime < 0 {
            destinationTime = 0
        }
        
        if destinationTime < duration.seconds {
            let newTime = CMTime(value: Int64(destinationTime * 1000 as Float64), timescale: 1000)
            player.seek(to: newTime)
        }
    }
    
    
    func playFromStart() {
        player.pause()
        player.seek(to: .zero)
        player.play()
    }
}


// MARK: -- MotionDataControlProcessorDelegate

extension EventControlPanel: MotionDataControlProcessorDelegate {
    func call(action: ControlAction) {
        switch action {
        case .replay:
            self.playFromStart()
            break
        case .encreaseVolume:
            self.increaseVolume()
            break
        case .decreaseVolume:
            self.decreaseVolume()
            break
        case .backward:
            seekPlayback(isForward: false)
            break
        case .forward:
            seekPlayback(isForward: true)
            break
        }
    }
    
}
