//
//  MathHelper.swift
//  EventInteractablePlayer
//
//  Created by Catherine Rohovska on 12.01.2022.
//

import Foundation

final class MathHelper {
    
    static func radiansToDegrees(_ radAngle: Double) -> Double {
        return radAngle * 180 / .pi
    }
    
}
